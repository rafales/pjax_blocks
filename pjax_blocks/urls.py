from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('pjax_blocks.views',
    url(r'^$', 'index', name="home"),
    url(r'^movies/$', 'movie_list', name="movie_list"),
    url(r'^movies/(?P<id>\d+)/$', 'movie_detail', name="movie_detail"),
    url(r'^admin/', include(admin.site.urls)),
)
