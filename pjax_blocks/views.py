from pjax_blocks.utils import render_pjax


def index(request):
    """
    Intro view.
    """
    return render_pjax(request, 'index.html')


def movie_list(request):
    """
    Lists movies.
    """
    object_list = range(1, 21)
    return render_pjax(request, 'movie_list.html', {'object_list': object_list})


def movie_detail(request, id):
    """
    Displays movie details.
    """
    object_list = range(1, 21)
    return render_pjax(request, 'movie_detail.html',
                       {'object': id, 'object_list': object_list})
