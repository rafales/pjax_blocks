from django import template
from django.template import TemplateSyntaxError
from django.template.loader_tags import BlockNode, BLOCK_CONTEXT_KEY
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe


register = template.Library()


class RegionNode(BlockNode):
    def __init__(self, name, nodelist, parent=None):
        super(RegionNode, self).__init__(name, nodelist, parent)

    def render(self, context, top=True):
        request = context['request']
        esc = conditional_escape
        contents = super(RegionNode, self).render(context)
        if top:
            params = (esc(self.name), esc(request.path), esc(contents))
            wrapper = '<div data-region="%s" data-url="%s">%s</div>'
            contents = mark_safe(wrapper % params)

        return contents

    def super(self):
        render_context = self.context.render_context
        if (BLOCK_CONTEXT_KEY in render_context and
            render_context[BLOCK_CONTEXT_KEY].get_block(self.name) is not None):
            return mark_safe(self.render(self.context))
        return ''


@register.tag('region')
def do_region(parser, token):
    """
    Defines a region that can be overridden in child templates and can be used
    with pjax to load only parts of a page.

    {% region user_detail %} Some details {% endregion user_detail %}
    """
    bits = token.contents.split()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' tag takes at least one argument" % bits[0])

    region_name = bits[1]
    # Keep track of the names of BlockNodes found in this template, so we can
    # check for duplication.
    try:
        if region_name in parser.__loaded_blocks:
            raise TemplateSyntaxError("'%s' tag with name '%s' appears more than once" % (bits[0], region_name))
        parser.__loaded_blocks.append(region_name)
    except AttributeError: # parser.__loaded_blocks isn't a list yet
        parser.__loaded_blocks = [region_name]
    nodelist = parser.parse(('endregion',))

    # This check is kept for backwards-compatibility. See #3100.
    endregion = parser.next_token()
    acceptable_endings = ('endregion', 'endregion %s' % region_name)
    if endregion.contents not in acceptable_endings:
        parser.invalid_block_tag(endregion, 'endregion', acceptable_endings)

    return RegionNode(region_name, nodelist)
