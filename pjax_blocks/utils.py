import json
from django.utils.encoding import force_text
from django.http import HttpResponseBadRequest, HttpResponse
from django.template import TextNode, loader, RequestContext
from django.template.loader_tags import ExtendsNode, BlockContext, BlockNode, BLOCK_CONTEXT_KEY
from pjax_blocks.templatetags.pjax_blocks import RegionNode


class UnknownRegionsError(Exception):
    """
    Raised when one or more regions do not exist.
    """
    def __init__(self, regions):
        self.regions = regions
        msg = "Unknown regions: %s" % ", ".join(regions)
        super(UnknownRegionsError, self).__init__(msg)


def find_extends_node(tpl):
    """
    Finds ExtendsNode in template or returns None.
    """
    for node in tpl.nodelist:
        if isinstance(node, ExtendsNode):
            return node
        elif isinstance(node, TextNode):
            continue
        else:
            return None


def collect_blocks(tpl, context):
    """
    Takes template, fills "block context" and returns it.
    """
    block_context = BlockContext()

    while True:
        extends = find_extends_node(tpl)

        if extends:
            block_context.add_blocks(extends.blocks)
            tpl = extends.get_parent(context)
        else:
            block_context.add_blocks({
            node.name: node for node in
            tpl.nodelist.get_nodes_by_type(BlockNode)})
            break

    return block_context


def render_regions(tpl, region_names, context):
    """
    Takes template, context and list of regions which needs to be rendered.
    """
    block_context = collect_blocks(tpl, context)

    # find region blocks
    regions = {name: nodes[-1] for name, nodes in block_context.blocks.items()
               if isinstance(nodes[-1], RegionNode)}

    # check if all blocks are available
    missing_regions = set(region_names) - set(regions.keys())
    if missing_regions:
        raise UnknownRegionsError(list(missing_regions))

    # we need to attach block context to template context just like "extends" does
    context.render_context[BLOCK_CONTEXT_KEY] = block_context

    # render regions
    ret = {}
    for name in region_names:
        ret[name] = regions[name].render(context)

    return ret


def render_pjax(request, template_name, context_dict=None):
    """
    A helper function for rendering pjax things.
    """
    regions = filter(None, request.GET.get('regions', '').split(','))
    tpl = loader.get_template(template_name)
    context = RequestContext(request, context_dict)

    if regions:
        try:
            data = render_regions(tpl, regions, context)
        except UnknownRegionsError as e:
            return HttpResponseBadRequest(force_text(e))
        return HttpResponse(json.dumps(data), content_type='application/json')

    return HttpResponse(tpl.render(context))
